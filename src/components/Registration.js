import React from 'react';
import './Registration.css';
import { throwStatement } from '@babel/types';
import Input from './Input';

class Registration extends React.Component {
  constructor(props) {
    super(props);

    // Initialize state
    this.state = {
      firstName: '',
      lastName: '',
      npiNumber: '',
      address: '',
      phoneNumber: '',
      emailAddress: '',
      isError: true,
      showMessage: false
    };
  }

  // Method to set state when changes happen
  handleChange = (name, value) => {
    this.setState({ [name]: value });
  };

  // Submits the form to some service
  submitForm = () => {
    // Should call some action with current state
    // someSubmitAction(this.state);

    // Show a confirmation to user
    this.setState({ showMessage: true });
    setTimeout(() => {
      this.setState({ showMessage: false });
    }, 3000);
  };

  // Method to render the submit button depending on error
  renderButton = () => {
    // Check to see if any field is empty
    if (
      this.state.firstName &&
      this.state.lastName &&
      this.state.npiNumber &&
      this.state.address &&
      this.state.phoneNumber &&
      this.state.emailAddress
    ) {
      // Renders active submit button
      return (
        <button className="ui primary button" onClick={this.submitForm}>
          Submit
        </button>
      );
    } else {
      // Renders inactive submit button
      return <button className="ui disabled button">Submit</button>;
    }
  };

  // Render message to user that the request has been submitted
  renderMessage = () => {
    // Checks to see if the page can show the message
    if (this.state.showMessage) {
      // Renders a success message to user
      return (
        <div>
          <h2 className="ui center aligned icon green header">
            <i className="check icon" />
            <div className="content">Submitted!</div>
          </h2>
        </div>
      );
    } else {
      // Render nothing
      return null;
    }
  };

  // Main render method
  render() {
    return (
      <div className="registration-form">
        <h1 className="ui header">
          User Registration
          <div className="sub header">
            Please fill out all of the information.
          </div>
        </h1>
        {this.renderMessage()}
        <div className="ui segment">
          <Input
            type="text"
            name="firstName"
            displayName="First Name"
            placeholder="Ex. John"
            handleChange={this.handleChange}
          />
          <Input
            type="text"
            name="lastName"
            displayName="Last Name"
            placeholder="Ex. Smith"
            handleChange={this.handleChange}
          />
          <Input
            type="text"
            name="npiNumber"
            displayName="NPI Number"
            placeholder="Ex. 1234567"
            handleChange={this.handleChange}
          />
          <Input
            type="text"
            name="address"
            displayName="Business Address"
            placeholder="Ex. 123 Wallaby Way"
            handleChange={this.handleChange}
          />
          <Input
            type="text"
            name="phoneNumber"
            displayName="Telephone Number"
            placeholder="Ex. 5555551234"
            handleChange={this.handleChange}
          />
          <Input
            type="text"
            name="emailAddress"
            displayName="Email Address"
            placeholder="Ex. john@john.com"
            handleChange={this.handleChange}
          />
        </div>
        {this.renderButton()}
      </div>
    );
  }
}

export default Registration;
