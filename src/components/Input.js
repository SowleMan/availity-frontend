import React from 'react';
import './Registration.css';

class Input extends React.Component {
  constructor(props) {
    super(props);

    // Initialize state
    this.state = {
      value: '',
      isError: false
    };
  }

  // Sets the state and checks for error on change, then promotes change to parent
  handleChange = event => {
    this.setState({ value: event.target.value });
    this.setState({ isError: !event.target.value });

    this.props.handleChange(event.target.name, event.target.value);
  };

  // Sets isError to true if user leaves the input empty
  handleBlur = event => {
    this.setState({ isError: !event.target.value });
  };

  // Renders the warning to the user that the input is empty
  renderWarning = () => {
    // Checks if the page has an error
    if (this.state.isError) {
      // Renders a warning label for user
      return (
        <div className="ui red left pointing basic label">
          Please enter a value
        </div>
      );
    } else {
      // Render nothing
      return null;
    }
  };

  // Main render method
  render() {
    return (
      <div className="input-section">
        <div>{this.props.displayName}</div>
        <div className="ui input">
          <input
            type={this.props.type}
            name={this.props.name}
            placeholder={this.props.placeholder}
            autoComplete="off"
            value={this.state.value}
            onChange={this.handleChange}
            onBlur={this.handleBlur}
          />
          {this.renderWarning()}
        </div>
      </div>
    );
  }
}

export default Input;
